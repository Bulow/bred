package com.tripequip;

import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.output.support.AbstractXMLOutputProcessor;
import org.jdom2.output.support.FormatStack;
import org.jdom2.output.support.XMLOutputProcessor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by emil on 2/10/16.
 */
public class ShopXmlGenerator
{
    private static String apiKey = "79CF52572D5357E4CB96B7A5";
    private static String createItemPrototype = "<createItem xmlns=\"http://www.cs.au.dk/dWebTek/2014\"><shopKey>%s</shopKey><itemName>%s</itemName></createItem>";
    protected static String generateCreateItemXml(String name)
    {
        return String.format(createItemPrototype, apiKey, name);
    }


    private static String modifyItemPrototype = "<modifyItem xmlns=\"http://www.cs.au.dk/dWebTek/2014\"><shopKey>%s</shopKey><itemID>%s</itemID><itemName>%s</itemName><itemPrice>%s</itemPrice><itemURL>%s</itemURL><itemDescription>%s</itemDescription></modifyItem>";
    protected static String generateModifyItemXml(String id, String name, String price, String url, String description)
    {
        return String.format(modifyItemPrototype, apiKey, id, name, price, url, description);
    }

    private static final XMLOutputProcessor noNamespaces = new AbstractXMLOutputProcessor() {
        @Override
        protected void printNamespace(Writer out, FormatStack fstack, Namespace ns) throws IOException {
            //Don't do it...
        }
    };

    public static String getContentAsString(Element e) throws Exception
    {
        XMLOutputter outputter = new XMLOutputter(noNamespaces);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        outputter.output(e.getContent(), stream);
        String content = stream.toString("UTF8");
        return content;
    }
}
