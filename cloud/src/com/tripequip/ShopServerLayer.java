package com.tripequip;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by emil on 2/11/16.
 */
public class ShopServerLayer
{
    public static void addItem(String name, String price, String url, String description) throws Exception
    {
        System.out.println("\rCreating item...");
        String newID = createItem("Tent", new URL(ShopUrls.baseUrl + ShopUrls.create));

        System.out.print("New item ID: ");
        System.out.println(newID);

        System.out.println("\rModifying item...");
        modifyItem(newID, name, price, url, description, new URL(ShopUrls.baseUrl + ShopUrls.modify));

    }

    public static String createItem(String name, URL createUrl) throws Exception
    {
        byte[] data = ShopXmlGenerator.generateCreateItemXml(name).getBytes();
        //System.out.write(data);
        //System.out.write('\r');

        Document response = httpPostXml(createUrl, data);
        if (response != null)
        {
            String newID = response.getRootElement().getText();
            return newID;
        }
        throw new Exception("Invalid response");
    }

    public static void modifyItem(String id, String name, String price, String url, String description, URL modifyUrl) throws Exception
    {
        byte[] data = ShopXmlGenerator.generateModifyItemXml(id, name, price, url, description).getBytes();
        //System.out.write(data);
        //System.out.write('\r');
        httpPostXml(modifyUrl, data);
    }


    private static Document httpPostXml(URL url, byte[] data) throws Exception
    {
        //Open connection with appropriate parameters
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();

        //Write POST data
        connection.getOutputStream().write(data);

        //Get response
        System.out.println(connection.getResponseMessage());
        if(connection.getResponseCode() == 200)
        {
            InputStream responseStream = connection.getInputStream();
            if (responseStream.available() > 0)
            {
                //Assume response is XML, if any
                Document response = new SAXBuilder().build(responseStream);
                connection.disconnect();
                return response;
            }
            return new Document();
        }

        connection.disconnect();
        throw new Exception(String.format("Post request to %s returned response code %s", url.toString(), connection.getResponseCode()));
    }
}
