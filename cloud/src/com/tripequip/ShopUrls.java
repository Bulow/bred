package com.tripequip;

/**
 * Created by emil on 2/11/16.
 */
public class ShopUrls
{
    public static final String baseUrl = "http://webtek.cs.au.dk/cloud";
    public static final String create = "/createItem";
    public static final String modify = "/modifyItem";
}
