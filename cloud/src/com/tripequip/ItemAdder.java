package com.tripequip;

import org.jdom2.*;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;

import java.io.File;

public class ItemAdder {

	public static void main(String[] args) {
		for (String path : args) {
			SAXBuilder builder = new SAXBuilder();
			try
			{
				Document doc = builder.build(new File(path));
				String itemName = doc.getRootElement().getDescendants(new ElementFilter("itemName")).next().getText();
				String itemUrl = doc.getRootElement().getDescendants(new ElementFilter("itemURL")).next().getText();
				String itemPrice = doc.getRootElement().getDescendants(new ElementFilter("itemPrice")).next().getText();

				//itemDescription contains an unknown number of children. Just copy the content as a string
				Element itemDescriptionElement = doc.getRootElement().getDescendants(new ElementFilter("itemDescription")).next();
				String itemDescription = ShopXmlGenerator.getContentAsString(itemDescriptionElement);

				ShopServerLayer.addItem(itemName, itemPrice, itemUrl, itemDescription);
			}
			catch (Exception ex)
			{
				System.out.println(ex.getMessage());
				continue;
			}


		}
	}






}
